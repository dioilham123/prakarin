<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PrakerinModel extends CI_Model {
	
	public function view() 
	{
		$this->db->select('*');
		$this->db->from('tb_prakerin');
		$this->db->join('tb_siswa','tb_siswa.nis=tb_prakerin.nis');
		$this->db->join('tb_jurusan','tb_jurusan.id_jurusan=tb_prakerin.id_jurusan');
		$this->db->join('tb_pembimbing','tb_pembimbing.id_pembimbing=tb_prakerin.id_pembimbing');
		$this->db->join('tb_perusahaan','tb_perusahaan.id_perusahaan=tb_prakerin.id_perusahaan');
		$query = $this->db->get();
		return $query->result();
	}
}