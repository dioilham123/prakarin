<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid box-warning">
      <div class="box-header">
        <center><h3 class="box-title">Data Prakerin</h3></center>
      </div>

      <div class="box-body table-responsive no-padding">
        <table class="table table-striped">

          <tr>
            <th>NIS</th>
            <th>Nama Siswa</th>
			<th>Jurusan</th>
			<th>Nama Pembimbing</th>
			<th>Nama Perusahaan</th>

          </tr>

          <?php
          if( ! empty($prakerin)){
            foreach($prakerin as $data){
              $id_prakerin = rawurlencode($data->id_prakerin);
              echo "<tr>
              <td>".$data->nis."</td>
              <td>".$data->nama_siswa."</td>
			  <td>".$data->nama_jurusan."</td>
			  <td>".$data->nama_pembimbing."</td>
			  <td>".$data->nama_perusahaan."</td>

      
              </tr>";
            }
          }else{
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
