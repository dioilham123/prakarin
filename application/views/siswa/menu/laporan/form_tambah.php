<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header  with-border">
        <center><h3 class="box-title">Input Data Laporan</h3></center>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <body>
          <div style="color: red;"><?php echo validation_errors(); ?></div>
          <?php echo form_open("siswa/laporanontroller/tambah"); ?>
            <table cellpadding="8">
	
		<tr>
            <div class="form-group has-success">
            <label class="control-label" for="inputSuccess">ID Laporan</label>
             <input type="text" class="form-control" name="input_id_laporan" id="inputSuccess" placeholder="ID_Laporan" value="<?php echo set_value('input_id_laporan'); ?>">
            </div>
        </tr>
		<tr>
            <div class="form-group has-success">
            <label class="control-label" for="inputSuccess">NIS</label>
             <input type="text" class="form-control" name="input_nis" id="inputSuccess" placeholder="NIS" value="<?php echo set_value('input_nis'); ?>">
            </div>
         </tr>
		 
		  <tr>
          <div class="form-group has-success">
           <label class="control-label" for="inputSuccess">Pembimbing</label>
            <input type="text" class="form-control" name="input_pembimbing" id="inputSuccess" placeholder="Pembimbing" value="<?php echo set_value('input_pembimbing'); ?>">
            </div>      
         </tr>
		<tr>
            <div class="form-group has-success">
            <label class="control-label" for="inputSuccess">Laporan</label>
				<?php echo form_open_multipart('siswa/laporancontroller/upload_file');?>
 
	<input type="file" name="berkas" />
			</div>
         </tr>

        
		 
	 </table>
            <input type="submit" class="btn btn-block btn-success" name="submit" value="Simpan">

            <hr>
            <a href="<?php echo base_url('/siswa/laporancontroller/'); ?>"><input class="btn btn-block btn-danger" type="button" value="Batal"></a>
          <?php echo form_close(); ?>
        </body>