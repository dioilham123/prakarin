<div class="row">
  <div class="col-xs-12">
    <a href='<?php echo base_url("siswa/laporancontroller/tambah"); ?>'><input class="btn btn-block btn-success btn-xs" type="button" value="Tambah"></a>
    <div class="box box-solid box-warning">
      <div class="box-header">
        <center><h3 class="box-title">Daftar Siswa</h3></center>
        <div class="box-tools">
        </div>
      </div>
      <!-- /.box-header -->

      <div class="box-body table-responsive no-padding">

        <table class="table table-striped">
          <tr>
            <th>ID Laporan</th>
            <th>NIS</th>
            <th>Pembimbing</th>
			<th>Laporan</th>
            <th colspan="2">Aksi</th>
          </tr>
          <?php
          if( ! empty($laporan)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
            foreach($laporan as $data){
              echo "<tr>
			  <td>".$data->id_laporan."</td>
              <td>".$data->nis."</td>
              <td>".$data->id_pembimbing."</td>
              <td>".$data->laporan."</td>



              <td><a href='".base_url("/siswa/laporancontroller/ubah/".$id_laporan)."'><button class='btn btn-block btn-success btn-xs' type='button' >Edit</button></a></td>
             
              </tr>";
            }
          }else{ // Jika data siswa kosong
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
