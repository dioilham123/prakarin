<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembimbingcontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('admin/PembimbingModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');
	$this->load->library('form_validation');

    if ($this->session->userdata('user_verifikasi')!="1") {
      redirect('logincontroller');
    }
  }

  public function index()
	{
    $data['header'] = 'Data Pembimbing';
    $data['content'] = 'admin/menu/pembimbing/view';
    $data['pembimbing'] = $this->PembimbingModel->view();
    $this->load->view('admin/home', $data);
	}
	
  public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->PembimbingModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->PembimbingModel->save(); // Panggil fungsi save() yang ada di SiswaModel.php
        redirect('admin/pembimbingcontroller');
      }
    }
    $data['header'] = 'Data Pembimbing';
    $data['content'] = 'admin/menu/pembimbing/form_tambah';
    $this->load->view('admin/home', $data);
  }

  public function hapus($id_pembimbing){
    $decoded_id_pembimbing = rawurldecode($id_pembimbing);
    $this->PembimbingModel->delete($decoded_id_pembimbing); // Panggil fungsi delete() yang ada di SiswaModel.php
    redirect('admin/pembimbingcontroller');
  }

  public function ubah($id_pembimbing){
    $decoded_id_pembimbing = rawurldecode($id_pembimbing);
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->PembimbingModel->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->PembimbingModel->edit($decoded_id_pembimbing); // Panggil fungsi edit() yang ada di SiswaModel.php
        redirect('admin/pembimbingcontroller');
      }
    }
    $data['header'] = 'Data Pembimbing';
    $data['content'] = 'admin/menu/pembimbing/form_ubah';
    $data['pembimbing'] = $this->PembimbingModel->view_by($decoded_id_pembimbing);
    $this->load->view('admin/home', $data);
  }
}
